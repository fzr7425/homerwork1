﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Unit1_EAP
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private string serviceUrl = "http://yottaclass.azurewebsites.net/";
        private WebClient request = new WebClient();
        private void button1_Click(object sender, EventArgs e)
        {
            label1.Text = "CallService";
            WebClient request = new WebClient();
            var result= request.DownloadData(Path.Combine(serviceUrl, "Home", "GetTextFile"));
            label1.Text = "Done";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            label1.Text = "CallService";
            WebClient request = new WebClient();
            request.DownloadDataCompleted += Request_DownloadDataCompleted;
            request.DownloadDataAsync(new Uri(Path.Combine(serviceUrl, "Home", "GetTextFile")));
        
        }

        private void Request_DownloadDataCompleted(object sender, DownloadDataCompletedEventArgs e)
        {
            label1.Text = "Done";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            WebClient request = new WebClient();
            request.DownloadDataCompleted += delegate(object sender1, DownloadDataCompletedEventArgs e1)
            {
                var file=System.Text.Encoding.UTF8.GetString(e1.Result);
                WebClient request2 = new WebClient();
                request2.DownloadDataCompleted += delegate (object sender2, DownloadDataCompletedEventArgs e2)
                {
                    var file2 = System.Text.Encoding.UTF8.GetString(e2.Result);
                    WebClient request3 = new WebClient();
                    request3.DownloadDataCompleted += delegate (object sender3, DownloadDataCompletedEventArgs e3)
                    {
                        label1.Text = "Done";
                    };
                    request3.DownloadDataAsync(new Uri(Path.Combine(serviceUrl, file2)));
                };
                request2.DownloadDataAsync(new Uri(Path.Combine(serviceUrl, file)));
            };
            request.DownloadDataAsync(new Uri(Path.Combine(serviceUrl, "Home", "GetTextFile")));
        }

        private void Request_DownloadDataCompleted1(object sender, DownloadDataCompletedEventArgs e)
        {
            
        }
    }
}
